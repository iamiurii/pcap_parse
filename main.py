from __future__ import print_function
from enum import Enum
import dpkt
import socket
import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument('capture', help='network capture to process')
args = parser.parse_args()


def inet_to_str(inet):
    """convert inet address to decimal representation
    """
    try:
        return socket.inet_ntop(socket.AF_INET, inet)
    except ValueError:
        return socket.inet_ntop(socket.AF_INET6, inet)


class StatRecordKey:
    """
    Represent SrcIp:SrcPort -> DstIp:DstPort session
    """
    def __init__(self, src_ip, dst_ip, src_port, dst_port):
        self.src_ip = src_ip
        self.dst_ip = dst_ip
        self.src_port = src_port
        self.dst_port = dst_port

    def __str__(self):
        return "{:>15} -> {:>15}:{:>5}".format(inet_to_str(self.src_ip), inet_to_str(self.dst_ip), self.dst_port)

    def __repr__(self):
        return "{} -> {}:{}".format(self.src_ip, self.dst_ip, self.dst_port)

    def __eq__(self, other):
        """Source port is not considered.
        """
        return self.src_ip == other.src_ip and self.dst_ip == other.dst_ip \
               and self.dst_port == other.dst_port

    def __hash__(self):
        return hash(repr(self))


class StatRecord:

    def __init__(self, tcp_session):
        self.tcp_session = tcp_session
        self.success = 0
        self.retransmit = 0
        self.reset = 0

    def __str__(self):
        return "{} {:>3} {:>3} {:>3} {:>3}".format(self.tcp_session, self.reset, self.retransmit,
                                       self.reset + self.retransmit, self.success)

    def __eq__(self, other):
        return self.tcp_session.src_ip == other.tcp_session.src_ip and \
               self.tcp_session.dst_ip == other.tcp_session.dst_ip and \
               self.tcp_session.dst_port == other.tcp_session.dst_port

    def __hash__(self):
        return hash(str(self))


class Parameter(Enum):
    RETRASNMISSION = 1
    RESET = 2
    SUCCESS = 3


class PcapStats:
    def __init__(self, file_path):
        self.file_path = file_path
        self.est_session_stats = {}

    def increment_stat(self, session, parameter):
        if session not in self.est_session_stats:
            self.est_session_stats[session] = StatRecord(session)

        if parameter == Parameter.RETRASNMISSION:
            self.est_session_stats[session].retransmit += 1
        elif parameter == Parameter.RESET:
            self.est_session_stats[session].reset += 1
        elif parameter == Parameter.SUCCESS:
            self.est_session_stats[session].success += 1

    def parse(self):
        """Iterate over all packets in capture.
        Filter TCP packet's.
        Distinguish the following packets:
        1. SYN
        2. SYN/ACK
        3. RST/ACK
        4. ACK
        """
        # Clean stats
        self.est_session_stats = {}

        with open(self.file_path, 'rb') as fd:
            pcap = dpkt.pcap.Reader(fd)

            syn_list = []
            syn_ack_list = []

            for ts, buf in pcap:
                eth_pkt = dpkt.sll.SLL(buf)

                if isinstance(eth_pkt.data, dpkt.ip.IP):
                    ip = eth_pkt.data
                    if isinstance(ip.data, dpkt.tcp.TCP):
                        tcp = ip.data

                        if tcp.flags & dpkt.tcp.TH_SYN and not tcp.flags & dpkt.tcp.TH_ACK:
                            """SYN packet
                            """
                            session = StatRecordKey(ip.src, ip.dst, tcp.sport, tcp.dport)
                            if session in syn_list:
                                self.increment_stat(session, Parameter.RETRASNMISSION)
                            else:
                                syn_list.append(session)

                        elif tcp.flags & dpkt.tcp.TH_SYN and tcp.flags & dpkt.tcp.TH_ACK:
                            """SYN/ACK packet.
                            """
                            session = StatRecordKey(ip.dst, ip.src, tcp.dport, tcp.sport)
                            if session in syn_list:
                                syn_ack_list.append(session)

                        elif tcp.flags & dpkt.tcp.TH_RST and tcp.flags & dpkt.tcp.TH_ACK:
                            """RST/ACK packet
                            """
                            session = StatRecordKey(ip.dst, ip.src, tcp.dport, tcp.sport)
                            if session in syn_list:
                                syn_list.remove(session)
                                self.increment_stat(session, Parameter.RESET)

                        elif tcp.flags & dpkt.tcp.TH_ACK and not tcp.flags & dpkt.tcp.TH_PUSH \
                                and not tcp.flags & dpkt.tcp.TH_RST and len(tcp.data) == 0:
                            """ACK packet
                            """
                            session = StatRecordKey(ip.src, ip.dst, tcp.sport, tcp.dport)
                            if session in syn_ack_list:
                                syn_list.remove(session)
                                syn_ack_list.remove(session)
                                self.increment_stat(session, Parameter.SUCCESS)

    def __str__(self):
        sb = list()
        sb.append('{:>15} -> {:>15} {:>5} {:>3} {:>3} {:>3} {:>3}\n'.format('SRC IP', 'DST IP',
                                                                            'PORT', 'RES', 'RET', 'ERR', 'OK'))
        for key, value in self.est_session_stats.items():
            sb.append('{}\n'.format(value))

        sb.append('\nRES - RST ACK packet after SYN sent (port closed etc)\n')
        sb.append('RET - retransmission of SYN packet (drop)\n')
        sb.append('ERR - RST + RET\n')
        sb.append('OK  - Successful SYN - SYN/ACK - ACK exchange\n')
        return ''.join(sb)


if __name__ == '__main__':
    pcap_stats = PcapStats(args.capture)
    try:
        pcap_stats.parse()
        print(pcap_stats)
    except IOError as error:
        print("File not found")
        sys.exit(1)
