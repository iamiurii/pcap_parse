# pcap_parse

## Usage:

```
python main.py capture.pcap

capture.pcap - network capture to process
```

## Dependencies:

```
dpkt
```

## Purpose:

Parses pcap file and shows some basic statisc's
```
         SRC IP ->          DST IP  PORT RES RET ERR  OK
     172.17.0.1 ->     172.17.0.14:  443   0   6   6   0
   172.17.0.254 ->      172.17.0.1:  443   0   0   0   3
  200.200.200.1 ->  200.200.200.50:  443   0   0   0   2
     172.17.0.1 ->     172.17.0.14:   80   0   4   4   0
     172.17.0.1 ->      172.17.0.2: 7754   2   0   2   0
     172.17.0.1 ->      172.17.0.2: 7755   3   0   3   0
     172.17.0.1 ->        10.0.0.1:   80   0   0   0   1
     172.17.0.1 ->       10.0.0.40:   80   0   0   0   1
     172.17.0.1 ->      172.17.0.2: 1234   2   0   2   0
     172.17.0.1 ->      172.17.0.2: 2904   8   0   8   0

RES - RST ACK packet after SYN sent (port closed etc)
RET - retransmission of SYN packet (drop)
ERR - RST + RET
OK  - Successful SYN - SYN/ACK - ACK exchange
```

## Limitiations:

Parses packet's wrapped into Linux cooked-mode capture (SLL):
```
eth_pkt = dpkt.sll.SLL(buf)
```